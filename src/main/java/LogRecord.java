import java.util.concurrent.atomic.AtomicLong;
import org.apache.ignite.cache.query.annotations.QuerySqlField;

/**
 * This class represents syslog record object.
 * @author aynur
 *
 */
public class LogRecord {
  private static final AtomicLong ID_GEN = new AtomicLong();

  /** Record ID (indexed). */
  @QuerySqlField(index = true)
  private Long id;
    
  /** Syslog priority. */
  @QuerySqlField
  private int priority;
    
  /** Record hour. */
  @QuerySqlField
  private int hour;
    
  public LogRecord() {
    // No-op.
  }
    
  /**
   * Constructor for LogRecord.
   * 
   * @param priority Syslog priority.
   * @param hour Record hour.
   */
  public LogRecord(int priority, int hour) {
    id = ID_GEN.incrementAndGet();
    this.priority = priority;
    this.hour = hour;
  }
    
  /**
   * 
   * @return Syslog priority.
   */
  public int priority() {
    return priority;
  }
    
  /**
   * 
   * @return Syslog record hour.
   */
  public int hour() {
    return hour;
  }
    
  /** 
   * {@inheritDoc} 
   */
  @Override 
  public String toString() {
    return "LogRecord [id=" + id + ", priority=" + priority 
      + ", hour=" + hour + ']';
  }
}
