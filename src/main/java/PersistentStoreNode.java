import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.IgniteDataStreamer;
import org.apache.ignite.Ignition;
import org.apache.ignite.cache.CacheAtomicityMode;
import org.apache.ignite.cache.CacheWriteSynchronizationMode;
import org.apache.ignite.configuration.CacheConfiguration;

/**
 * Class for import data to persistence. 
 */
public class PersistentStoreNode {
  /**
   * @param args.
   */
  public static void main(String[] args) {
    try (Ignite ignite = Ignition.start("config.xml")) {
      CacheConfiguration<Long, LogRecord> cacheCfg = new CacheConfiguration<>("streamCache");
      ignite.active(true);
      cacheCfg.setAtomicityMode(CacheAtomicityMode.TRANSACTIONAL);
      cacheCfg.setBackups(1);
      cacheCfg.setWriteSynchronizationMode(CacheWriteSynchronizationMode.FULL_SYNC);
      cacheCfg.setIndexedTypes(Long.class, LogRecord.class);
      IgniteCache<Long, LogRecord> cache = ignite.getOrCreateCache(cacheCfg);
      cache.clear();
      try (IgniteDataStreamer<Long, LogRecord> stmr = ignite.dataStreamer("streamCache")) {    
        // Stream entries.
        File file = new File("input/syslog");
        System.out.println("Reading the file...");
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
          stmr.allowOverwrite(true);
          long l = 0L;
          String[] lines;
          String hour;
          String line;
          while ((line = br.readLine()) != null) {
            lines = line.split(" ");
            hour = lines[4].split(":")[0];
            stmr.addData(l, new LogRecord(Integer.parseInt(lines[0]), 
                 Integer.parseInt(hour)));
            l++;
          }
          System.out.println("File is read... ");
        } catch (FileNotFoundException e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
        } catch (IOException e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
        }
      }
    }
  }
}
