import java.util.Map;
import java.util.TreeMap;

/**
 * Class for storage information about message's priority level and their counts for hour. 
 * 
 * @author aynur.
 */
public class CountedRecord {
  private int hour; //first member of pair
  private Map<Integer, Long> map; //second member of pair

  public CountedRecord(int hour, Map<Integer, Long> map) {
    this.hour = hour;
    this.map = map;
  }

  /**
   * @param hour An hour of syslog messages.
   * @param priority Syslog priority level.
   * @param count Count of syslog messages of priority level for this hour.
   */
  public CountedRecord(int hour, int priority, long count) {
    this.hour = hour;
    map = new TreeMap<Integer, Long>();
    map.put(priority, count);
  }
  
  /**
   * @param log LogRecord instance.
   */
  public CountedRecord(LogRecord log) {
    hour = log.hour();
    map = new TreeMap<Integer, Long>();
    map.put(log.priority(), 1L);
  }

  /**
   * @return An hour.
   */
  public int getHour() {
    return hour;
  }

  /**
   * @return a map.
   */
  public Map<Integer, Long> getMap() {
    return map;
  }

  /**
   * Method for merging two CountedRecord instances.
   * 
   * @param record a CountedRecord instances to merge.
   */
  public void merge(CountedRecord record) {
    if (hour == record.getHour()) {
      for (Integer key : record.getMap().keySet()) {
        long count = record.getMap().get(key);
        if (map.containsKey(key)) {
          map.put(key, map.get(key) + count);
        } else {
          map.put(key, count);
        }
      }
    }
  }

  @Override
  public String toString() {
    return "CountedRecord [hour=" + hour + ", map=" + map + "]";
  }
}
