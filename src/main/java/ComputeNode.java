import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.IgniteCompute;
import org.apache.ignite.IgniteException;
import org.apache.ignite.Ignition;
import org.apache.ignite.cache.query.QueryCursor;
import org.apache.ignite.cache.query.SqlFieldsQuery;
import org.apache.ignite.compute.ComputeJob;
import org.apache.ignite.compute.ComputeJobAdapter;
import org.apache.ignite.compute.ComputeJobResult;
import org.apache.ignite.compute.ComputeTaskSplitAdapter;

/**
 * Starts up an empty node with compute configuration.
 */
public class ComputeNode {
  /**
   * Start up an empty node with compute configuration.
   *
   * @param args Command line arguments, none required.
   * @throws IgniteException If failed.
   */
  public static void main(String[] args) throws IgniteException {
    try (Ignite ignite = Ignition.start("config.xml")) {
      ignite.active(true);
      IgniteCache<Long, LogRecord> cache = ignite.cache("streamCache");
      IgniteCompute compute = ignite.compute();
      SqlFieldsQuery sql = new SqlFieldsQuery("select priority, hour from LogRecord");
      try (QueryCursor<List<?>> cursor = cache.query(sql)) {
        List<LogRecord> records = new ArrayList<LogRecord>();
        for (List<?> row : cursor) {
          LogRecord log = new LogRecord((Integer) row.get(0), (Integer) row.get(1));
          records.add(log);
        }
        // Execute task on the cluster and wait for its completion.
        Map<Integer, CountedRecord> map = compute.execute(RecordsCountTask.class, records);

        for (CountedRecord cr : map.values()) {
          System.out.println(cr.getHour() + "ый час:");
          for (Integer key : cr.getMap().keySet()) {
            System.out.println("  " + convertPriorityName(key) + ": " + cr.getMap().get(key));
          }
        }
      }
    }
  }


  /**
   * Task to count syslog priority count in hour.
   */
  private static class RecordsCountTask 
      extends ComputeTaskSplitAdapter<List<LogRecord>, Map<Integer, CountedRecord>> {
    
    private static final long serialVersionUID = 1L;

    // 1. Splits the received list of records into to LogRecord instances
    // 2. Creates a child job for each record
    // 3. Sends created jobs to other nodes for processing. 
    @Override 
    protected Collection<? extends ComputeJob> split(int clusterSize, List<LogRecord> arg) {
      List<ComputeJob> jobs = new ArrayList<>(arg.size());
      for (final LogRecord log : arg) {
        jobs.add(new ComputeJobAdapter() {
          private static final long serialVersionUID = 1L;
          @Override 
          public Object execute() {
            // Return LogRecord.
            return new CountedRecord(log);
          }
        });
      }
      return jobs;
    }

    @Override 
    public Map<Integer, CountedRecord> reduce(List<ComputeJobResult> results) {
      Map<Integer, CountedRecord> map = new TreeMap<Integer, CountedRecord>();
      for (ComputeJobResult res : results) {
        CountedRecord cr = res.<CountedRecord>getData();
        int key = cr.getHour();
        if (map.containsKey(key)) {
          cr.merge(map.get(key));
          map.put(key, cr);
        } else {
          map.put(key, cr);
        }
      }
      return map;
    }
  }
  
  /**
   * @param priority number.
   * @return a String, name of priority.
   */
  public static String convertPriorityName(int priority) {
    String priorityName;
    switch (priority) {
      case 0:
        priorityName = "Emergency";
        break;
      case 1:
        priorityName = "Alert";
        break;
      case 2:
        priorityName = "Critical";
        break;
      case 3:
        priorityName = "Error";
        break;
      case 4:
        priorityName = "Warning";
        break;
      case 5:
        priorityName = "Notice";
        break;
      case 6:
        priorityName = "Informational";
        break;
      case 7:
        priorityName = "Debug";
        break;
      default:
        priorityName = null;
    }
    return priorityName;
  }
}