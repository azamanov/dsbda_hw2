import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * @author aynur.
 *
 */
public class LogRecordTest {

  /**
   * Test method for {@link LogRecord#priority()}.
   */
  @Test
  public final void testPriority() {
    LogRecord log = new LogRecord(1, 5);
    assertTrue(log.priority() == 1);
  }

  /**
   * Test method for {@link LogRecord#hour()}.
   */
  @Test
  public final void testHour() {
    LogRecord log = new LogRecord(2, 1);
    assertTrue(log.hour() == 1);
  }
}
