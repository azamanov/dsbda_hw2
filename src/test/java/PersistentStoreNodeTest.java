import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.List;

import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.Ignition;
import org.apache.ignite.cache.query.QueryCursor;
import org.apache.ignite.cache.query.SqlFieldsQuery;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author aynur.
 *
 */
public class PersistentStoreNodeTest {

  static Ignite ignite;
  static IgniteCache<Long, LogRecord> cache;
  static File file;
  
  /**
   * @throws java.lang.Exception.
   * 
   */
  @BeforeClass
  public static void setUp() throws Exception {
    OutputStream os = new ByteArrayOutputStream();
    System.setOut(new PrintStream(os));
    PersistentStoreNode.main(null);
    ignite = Ignition.start("config.xml");
    ignite.active(true);
    cache = ignite.cache("streamCache");
    file = new File("input/syslog");
  }

  /**
   * @throws java.lang.Exception.
   * 
   */
  @AfterClass
  public static void tearDown() throws Exception {
    ignite.close();
    System.setOut(System.out);
  }

  /**
   * Test method for {@link PersistentStoreNode#main(java.lang.String[])}.
   * Check that number of lines in syslog file equals to records number at cache.
   */
  @Test
  public final void testMainCount() {
    int rowCount = 0;
    String line;
    try (BufferedReader br = new BufferedReader(new FileReader(file))) {
      while ((line = br.readLine()) != null) {
        rowCount++;
      }
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
    SqlFieldsQuery sql = new SqlFieldsQuery("select count(id) from LogRecord");
    QueryCursor<List<?>> cursor = cache.query(sql);
    int count = (int) (long) cursor.getAll().get(0).get(0);
    assertTrue(count == rowCount);
  }
  
  /**
   * Test method for {@link PersistentStoreNode#main(java.lang.String[])}.
   * Check that the fourth line of syslog is correctly represented at Ignite Persistence. 
   */
  @Test
  public final void testMainRecord() {
    int rowCount = 0;
    LogRecord fourthRecord = null;
    String line;
    try (BufferedReader br = new BufferedReader(new FileReader(file))) {
      while ((line = br.readLine()) != null) {
        rowCount++;
        if (rowCount == 4) {
          String[] lines = line.split(" ");
          fourthRecord = 
            new LogRecord(Integer.parseInt(lines[0]), Integer.parseInt(lines[4].split(":")[0]));
        }
      }
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
    SqlFieldsQuery sql = new SqlFieldsQuery("select priority, hour from LogRecord where id=4");
    QueryCursor<List<?>> cursor = cache.query(sql);
    List<?> row = cursor.getAll().get(0);
    assertTrue(fourthRecord.hour() == (int) row.get(1));
    assertTrue(fourthRecord.priority() == (int) row.get(0));
  }
}
