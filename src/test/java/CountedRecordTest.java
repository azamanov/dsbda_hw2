import static org.junit.Assert.assertTrue;

import java.util.Map;
import java.util.TreeMap;
import org.junit.Test;

/**
 * @author aynur.
 *
 */
public class CountedRecordTest {

  /**
   * Test method for {@link CountedRecord#getHour()}.
   */
  @Test
  public void testGetHour() {
    CountedRecord cr = new CountedRecord(1, 3, 1L);
    assertTrue(cr.getHour() == 1);
  }

  /**
   * Test method for {@link CountedRecord#getMap()}.
   */
  @Test
  public void testGetMap() {
    CountedRecord cr = new CountedRecord(1, 3, 1L);
    Map<Integer, Long> map = new TreeMap<Integer, Long>();
    map.put(3, 1L);
    assertTrue(cr.getMap().equals(map));
  }

  /**
   * Test method for {@link CountedRecord#merge(CountedRecord)}.
   */
  @Test
  public void testMerge() {
    CountedRecord cr1 = new CountedRecord(1, 3, 1L);
    CountedRecord cr2 = new CountedRecord(1, 2, 5L);
    CountedRecord cr3 = new CountedRecord(2, 3, 1L);
    CountedRecord cr4 = new CountedRecord(2, 3, 2L);
    cr1.merge(cr2);
    cr3.merge(cr4);
    Map<Integer, Long> map = new TreeMap<Integer, Long>();
    map.put(3, 1L);
    map.put(2, 5L);
    Map<Integer, Long> map2 = new TreeMap<Integer, Long>();
    map2.put(3, 3L);
    assertTrue(cr1.getHour() == 1);
    assertTrue(cr3.getHour() == 2);
    assertTrue(cr1.getMap().equals(map));
    assertTrue(cr3.getMap().equals(map2));
  }
}
