import static org.junit.Assert.assertTrue;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;

import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author aynur.
 *
 */
public class ComputeNodeTest {

  Ignite ignite;
  IgniteCache<Long, LogRecord> cache;
  File file;
  OutputStream os;
  PrintStream ps;
  
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
    PersistentStoreNode.main(null);
  }
  
  /**
   * @throws java.lang.Exception
   * 
   */
  @Before
  public void setUp() throws Exception {
    file = new File("input/syslog");

    os = new ByteArrayOutputStream();
    ps = new PrintStream(os);
    System.setOut(ps);
  }

  /**
   * @throws java.lang.Exception
   * 
   */
  @After
  public void tearDown() throws Exception {
    System.setOut(System.out);
  }

  /**
   * Test method for {@link ComputeNode#main(java.lang.String[])}.
   */
  @Test
  public final void testMain() {
    try (PrintWriter writer = new PrintWriter(file)) {
      writer.println("4 3 23 Nov 15:51:32 System message");
      writer.println("4 2 23 Nov 15:57:24 New Message");
      writer.println("5 5 23 Nov 16:02:08 New Message");
      writer.println("6 1 23 Nov 16:03:22 New Message");
      writer.println("6 7 23 Nov 16:30:54 New Message");
      writer.println("1 3 23 Nov 16:47:18 New Message");
      writer.println("2 1 23 Nov 17:00:44 New Message");
    } catch (FileNotFoundException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    PersistentStoreNode.main(null);
    ComputeNode.main(null);
    StringBuilder sb = new StringBuilder();
    sb.append("15ый час:\n");
    sb.append("  Warning: 2\n");
    sb.append("16ый час:\n");
    sb.append("  Alert: 1\n");
    sb.append("  Notice: 1\n");
    sb.append("  Informational: 2\n");
    sb.append("17ый час:\n");
    sb.append("  Critical: 1\n");
    assertTrue(os.toString().contains(sb.toString()));
  
  }

  /**
   * Test method for {@link ComputeNode#convertPriorityName(int)}.
   */
  @Test
  public final void testConvertPriorityName() {
   //fail("Not yet implemented"); // TODO
  }

}
